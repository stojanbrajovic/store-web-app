const BASE_URL = "http://localhost:8081";
const TOKEN_ITEM_NAME = "token";

const getAuthToken = () => {
	return localStorage.getItem(TOKEN_ITEM_NAME);
};

const getHeaders = () => ({
	Authorization: getAuthToken() || ""
});

export enum Method {
	POST = "POST",
	GET = "GET",
	DELETE = "DELETE"
}

export const send = async (
	method: Method,
	url: string,
	data: object = null
) => {
	const init: RequestInit = {
		headers: getHeaders(),
		method
	};
	if (data) {
		init.body = JSON.stringify(data);
	}
	const response = await fetch(BASE_URL + "/" + url, init);
	checkResponseStatus(response);
	const json = await response.json();
	return json;
};

const checkResponseStatus = (response: Response) => {
	// raises an error in case response status is not a success
	if (response.status >= 200 && response.status < 300) {
		// Success status lies between 200 to 300
		return response;
	} else {
		const error = new Error(response.statusText);
		(error as any).response = response; // TODO check if this can be done better
		throw error;
	}
};

export default send;
