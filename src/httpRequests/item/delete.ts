import { send, Method } from "../send";

const deleteItem = async (id: string) => {
	const result = await send(Method.DELETE, `item?id=${id}`);
	return result;
};

export default deleteItem;
