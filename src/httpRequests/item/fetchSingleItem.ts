import { send, Method } from "../send";
import Item from "../../models/Item";
import StoreObject from "../../models/StoreObject";

interface FetchSingleItemResponse {
	item: Item;
	childItems: Item[];
	parameterValues: StoreObject[];
	parameters: StoreObject[];
}

const fetchSingleItem = async (id: string) => {
	const url = `fullItemData?id=${id}`;
	const result: FetchSingleItemResponse = await send(Method.GET, url);
	const item: Item = { ...result.item, isFullData: true };
	return item;
};

export default fetchSingleItem;
