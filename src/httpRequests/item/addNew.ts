import { send, Method } from "../send";
import Item from "../../models/Item";

const addItem = async (item: Item) => {
	if (item.id) {
		throw new Error("trying to create item with id");
	}

	if (!item.name) {
		throw new Error("trying to create an item without name");
	}

	const id = await send(Method.POST, "item", item);
	return id;
};

export default addItem;
