import { send, Method } from "../send";
import Item from "../../models/Item";

const fetchStoreItems = async () => {
	const result: Item[] = await send(Method.GET, "storeItems");
	return result;
};

export default fetchStoreItems;
