import send, { Method } from "./send";
import { setToken } from "../authentication";

const login = async (username: string, password: string) => {
	const token: string = await send(Method.POST, "login", {
		username,
		password
	});
	setToken(token);
};

export default login;
