import Item from "../models/Item";
import User from "../models/User";

export const initialState = {
	isLoading: false,
	items: [] as Item[],
	rootItems: null as Item[],
	user: null as User
};

type State = typeof initialState;
export default State;
