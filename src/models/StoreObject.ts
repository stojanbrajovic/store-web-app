interface StoreObject {
	id?: string;
	name?: string;
	description?: string;
}

export default StoreObject;
