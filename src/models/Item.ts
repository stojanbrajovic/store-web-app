import StoreObject from "./StoreObject";

interface Item extends StoreObject {
	isFullData?: true;
	parameterIds?: string[];
	parameterValueIds?: string[];
}

export default Item;
