import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";

import Main from "./components/Main";
import registerServiceWorker from "./registerServiceWorker";
import { initialState } from "./redux/State";
import reducer from "./redux/reducer";

import "./index.scss";

const store = createStore(reducer, initialState);

ReactDOM.render(
	<Provider store={store}>
		<Main />
	</Provider>,
	document.getElementById("root") as HTMLElement
);
registerServiceWorker();
