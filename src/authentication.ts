const TOKEN_KEY = "token_key";

export const getToken = () => {
	return localStorage.getItem(TOKEN_KEY);
};

export const setToken = (token: string) => {
	localStorage.setItem(TOKEN_KEY, token);
};
