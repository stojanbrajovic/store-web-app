const routes = {
	ITEM: "/item",
	ADD_ITEM: "/addItem",
	LOGIN: "/login"
};

export default routes;
