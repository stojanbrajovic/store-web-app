import * as React from "react";

import Input from "./baseComponents/Input";
import Dialog from "./baseComponents/Dialog";

interface Properties {
	isOpen: boolean;
	onClose: VoidFunction;
	onLogin: (username: string, password: string) => void;
}

interface LoginState {
	username: string;
	password: string;
}

class LoginDialog extends React.Component<Properties, LoginState> {
	state: LoginState = { username: "", password: "" };

	render() {
		const { username, password } = this.state;
		const { isOpen, onClose } = this.props;

		return (
			<Dialog
				title="Login"
				isOpen={isOpen}
				onClose={onClose}
				onOk={this.onLogin}
				okButtonLabel="Login"
			>
				<Input
					value={username}
					placeholder="Username"
					onChange={this.onUsernameChanged}
					autoComplete={true}
				/>
				<div style={{ height: "2em" }} />
				<Input
					value={password}
					onChange={this.onPasswordChange}
					type="password"
					placeholder="Password"
					autoComplete={true}
				/>
			</Dialog>
		);
	}

	private onUsernameChanged = (username: string) => {
		this.setState({ username });
	};

	private onPasswordChange = (password: string) => {
		this.setState({ password });
	};

	private onLogin = () => {
		const { username, password } = this.state;
		this.props.onLogin(username, password);
	};
}

export default LoginDialog;
