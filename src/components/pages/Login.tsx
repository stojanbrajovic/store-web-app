import * as React from "react";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";

import Input from "../baseComponents/Input";
import Button from "../baseComponents/Button";
import ContentDiv from "../FullScreenCenteredContentDiv";
import setState from "../../redux/actions/setState";
import login from "../../httpRequests/login";

interface Properties {
	onLogin: (username: string, password: string) => void;
}

interface LoginState {
	username: string;
	password: string;
}

class Login extends React.Component<
	Properties & RouteComponentProps<{}>,
	LoginState
> {
	state: LoginState = { username: "", password: "" };

	render() {
		const { username, password } = this.state;

		return (
			<ContentDiv>
				<form style={{ width: "20em" }}>
					<Input
						value={username}
						onChange={this.onUsernameChanged}
						autoComplete={true}
					/>
					<Input
						value={password}
						onChange={this.onPasswordChange}
						type="password"
						autoComplete={true}
					/>
					<Button
						type="primary"
						style={{ width: "100%", marginTop: "1em" }}
						onClick={this.onLogin}
						disabled={!username || !password}
					>
						Login
					</Button>
				</form>
			</ContentDiv>
		);
	}

	private onUsernameChanged = (username: string) => {
		this.setState({ username });
	};

	private onPasswordChange = (password: string) => {
		this.setState({ password });
	};

	private onLogin = async () => {
		const { username, password } = this.state;
		await this.props.onLogin(username, password);
	};
}

const dispatchToProps = (
	dispatch: Dispatch,
	ownProps: RouteComponentProps<{}>
) => ({
	onLogin: async (username: string, password: string) => {
		setState(dispatch, { isLoading: true });
		try {
			await login(username, password);
			ownProps.history.goBack();
			setState(dispatch, { isLoading: false, user: { username } });
		} catch (error) {
			setState(dispatch, { isLoading: false });
			// tslint:disable-next-line:no-console
			console.error(error);
		}
	}
});

export default connect(
	null,
	dispatchToProps
)(Login);
