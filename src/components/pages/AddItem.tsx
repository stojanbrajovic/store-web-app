import * as React from "react";
import { Dispatch } from "redux";
import { connect } from "react-redux";

import Item from "../../models/Item";
import Input from "../baseComponents/Input";
import Button from "../baseComponents/Button";
import addNewItem from "../../httpRequests/item/addNew";
import createAction from "../../redux/actions/createAction";

interface OwnProperites {
	parentId?: string;
}

interface DispatchProps {
	addItem: (item: Item) => void;
}

type Properties = OwnProperites & DispatchProps;

class AddItem extends React.Component<Properties, Item> {
	state: Item = { isFullData: true };

	render() {
		return (
			<>
				{this.renderItemInputField("name")}
				{this.renderItemInputField("description")}
				<Button onClick={this.addItem}>Add</Button>
			</>
		);
	}

	private addItem = () => {
		this.props.addItem(this.state);
	};

	private renderItemInputField = (key: keyof Item) => {
		const item = this.state;
		const onChange = (value: string) => {
			this.setState({ ...item, [key]: value });
		};

		return (
			<Input
				value={item[key] ? item[key] + "" : ""}
				placeholder={key}
				onChange={onChange}
			/>
		);
	};
}

const dispatchToProps = (dispatch: Dispatch) => ({
	addItem: async (item: Item) => {
		dispatch(createAction(state => ({ ...state, isLoading: true })));
		const id = await addNewItem(item);
		alert(id);
		dispatch(
			createAction(state => {
				const nextState = { ...state, isLoading: false };
				nextState.items.push(item);
				// if (!parentElement)
				if (nextState.rootItems) {
					nextState.rootItems.push(item);
				}
				return nextState;
			})
		);
	}
});

export default connect(
	null,
	dispatchToProps
)(AddItem);
