import * as React from "react";
import { Dispatch } from "redux";
import { RouteComponentProps } from "react-router-dom";
import { connect } from "react-redux";

import ItemModel from "../../models/Item";
import fetchSingleItem from "../../httpRequests/item/fetchSingleItem";
import State from "../../redux/State";
import Button from "../baseComponents/Button";
import asyncDeleteItem from "../../httpRequests/item/delete";
import setState from "../../redux/actions/setState";
import createAction from "../../redux/actions/createAction";

type OwnProps = RouteComponentProps<{ itemId: string }>;

interface StateProps {
	item: ItemModel;
}

interface DispatchProps {
	loadItem: (itemId: string) => void;
	deleteItem: (itemId: string) => void;
}

type Properties = OwnProps & StateProps & DispatchProps;

const Item: React.SFC<Properties> = props => {
	const { item, loadItem, deleteItem } = props;
	if (!item || !item.isFullData) {
		loadItem(props.match.params.itemId);
		return null;
	}

	const onDelete = async () => {
		deleteItem(item.id);
		props.history.push(""); // go to homepage...
	};

	return (
		<>
			<h1>{item.name}</h1>
			<h3>{item.description || ""}</h3>
			<Button onClick={onDelete}>Delete</Button>
		</>
	);
};

const stateToProps = (state: State, ownProps: OwnProps) => ({
	item: state.items.find(item => item.id === ownProps.match.params.itemId)
});

const dispatchToProps = (dispatch: Dispatch): DispatchProps => ({
	loadItem: async (id: string) => {
		setState(dispatch, { isLoading: true });
		const item = await fetchSingleItem(id);
		dispatch(
			createAction(state => ({
				...state,
				isLoading: false,
				items: state.items.filter(i => i.id !== id).concat([item])
			}))
		);
	},
	deleteItem: async (id: string) => {
		setState(dispatch, { isLoading: true });
		await asyncDeleteItem(id);
		dispatch(
			createAction(state => ({
				...state,
				isLoading: false,
				items: state.items.filter(i => i.id !== id),
				rootItems: state.rootItems
					? state.rootItems.filter(i => i.id !== id)
					: null
			}))
		);
	}
});

export default connect<StateProps, DispatchProps, OwnProps>(
	stateToProps,
	dispatchToProps
)(Item);
