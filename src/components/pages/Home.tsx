import * as React from "react";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import List from "../baseComponents/List";
import Item from "../../models/Item";
import fetchStoreItems from "../../httpRequests/item/fetchStoreItems";
import State from "../../redux/State";
import createAction from "../../redux/actions/createAction";
import StoreAction from "../../redux/actions/StoreAction";
import Button from "../baseComponents/Button";
import routes from "../routes";

interface Properties {
	items: Item[];
	loadItems: VoidFunction;
}

const HomePage: React.SFC<Properties> = ({ items, loadItems }) => {
	if (!items) {
		loadItems();
	}
	return (
		<>
			<Link to={routes.ADD_ITEM}>
				<Button>Add Item</Button>
			</Link>
			<List
				items={(items || []).map(({ name, description, id }) => ({
					id,
					description,
					title: name,
					url: `${routes.ITEM}/${id}`
				}))}
			/>
		</>
	);
};

const stateToProps = ({ rootItems }: State) => {
	return { items: rootItems };
};

const dispatchToProps = (dispatch: Dispatch<StoreAction>) => ({
	loadItems: async () => {
		dispatch(
			createAction(state => ({
				...state,
				isLoading: true,
				rootItems: []
			}))
		);
		const rootItems = await fetchStoreItems();
		dispatch(
			createAction(state => ({
				...state,
				isLoading: false,
				rootItems,
				items: rootItems
					.filter(rootItem =>
						state.items.find(item => item.id === rootItem.id)
					)
					.concat(rootItems)
			}))
		);
	}
});

export default connect(
	stateToProps,
	dispatchToProps
)(HomePage);
