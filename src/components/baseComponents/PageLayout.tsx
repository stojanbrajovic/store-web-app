import * as React from "react";
import Layout from "antd/lib/layout/layout";
import "antd/lib/layout/style/css";
import Button from "./Button";

interface Properties {
	storeName: string;
	onLoginClick: VoidFunction;
	username: string;
}

const Header: React.SFC<Properties> = props => (
	<div style={{ display: "flex", justifyContent: "space-between" }}>
		<h1 style={{ color: "white" }}>{props.storeName}</h1>
		<span>
			{props.username || (
				<Button onClick={props.onLoginClick}>Login</Button>
			)}
		</span>
	</div>
);

const PageLayout: React.SFC<Properties> = props => (
	<Layout>
		<Layout.Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
			<Header {...props} />
		</Layout.Header>
		<Layout.Content style={{ padding: "0 50px", marginTop: 64 }}>
			{props.children}
		</Layout.Content>
	</Layout>
);

export default PageLayout;
