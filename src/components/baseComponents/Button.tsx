import * as React from "react";
import AntButton from "antd/lib/button/button";
import "antd/lib/button/style/css";

interface Properties {
	onClick?: VoidFunction;
	type?: "primary";
	style?: React.CSSProperties;
	disabled?: boolean;
}

const Button: React.SFC<Properties> = props => <AntButton {...props} />;

export default Button;
