import * as React from "react";
import AntList from "antd/lib/list";
import "antd/lib/list/style/css";

import { Link } from "react-router-dom";

interface ListItem {
	id: string;
	title: string;
	description?: string;
	url?: string;
}

interface Properties {
	items: ListItem[];
}

const ListItem: React.SFC<ListItem> = item => (
	<Link to={item.url}>
		<AntList.Item>
			<AntList.Item.Meta {...item} />
		</AntList.Item>
	</Link>
);
ListItem.defaultProps = {
	url: ""
};

const List: React.SFC<Properties> = props => (
	<AntList dataSource={props.items} renderItem={ListItem} />
);

export default List;
