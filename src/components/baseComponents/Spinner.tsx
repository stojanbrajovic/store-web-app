import * as React from "react";
import Spin, { SpinSize } from "antd/lib/spin";
import "antd/lib/spin/style/css";

interface Properties {
	size: SpinSize;
}

const Spinner: React.SFC<Properties> = ({ size }) => <Spin size={size} />;

export default Spinner;
