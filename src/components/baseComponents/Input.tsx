import * as React from "react";
import AntInput from "antd/lib/input";
import "antd/lib/input/style/css";

interface Properties {
	value: string;
	onChange: (val: string) => void;
	placeholder?: string;
	type?: "password";
	autoComplete?: boolean;
}

const Input: React.SFC<Properties> = props => {
	const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		props.onChange(event.currentTarget.value);
	};

	return (
		<AntInput
			{...{ ...props, onChange }}
			autoComplete={props.autoComplete ? "on" : "off"}
		/>
	);
};

export default Input;
