import * as React from "react";
import Modal from "antd/lib/modal";
import "antd/lib/modal/style/css";

interface Properties {
	title: string;
	isOpen: boolean;
	onClose: VoidFunction;
	onOk: VoidFunction;
	okButtonLabel?: string;
}

const Dialog: React.SFC<Properties> = props => (
	<Modal
		title={props.title}
		visible={props.isOpen}
		okButtonProps={{ onClick: props.onOk }}
		cancelButtonProps={{
			style: { display: "none" }
		}}
		onCancel={props.onClose}
		okText={props.okButtonLabel}
	>
		{props.children}
	</Modal>
);
Dialog.defaultProps = {
	okButtonLabel: "OK"
};

export default Dialog;
