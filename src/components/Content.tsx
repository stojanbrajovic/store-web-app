import * as React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Item from "./pages/Item";
import AddItem from "./pages/AddItem";
import Login from "./pages/Login";
import routes from "./routes";

const Content: React.SFC = () => (
	<Switch>
		<Route path={`${routes.ITEM}/:itemId`} component={Item} />
		<Route path={routes.ADD_ITEM} component={AddItem} />
		<Route path={routes.LOGIN} component={Login} />
		<Route path={""} component={Home} />
	</Switch>
);

export default Content;
