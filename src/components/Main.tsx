import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { BrowserRouter } from "react-router-dom";

import ReduxState from "../redux/State";
import Content from "./Content";
import Spinner from "./baseComponents/Spinner";
import ContentDiv from "./FullScreenCenteredContentDiv";
import PageLayout from "./baseComponents/PageLayout";
import LoginDialog from "./LoginDialog";
import login from "../httpRequests/login";
import setState from "../redux/actions/setState";
import User from "../models/User";

interface Properties {
	user: User;
	isLoading: boolean;
	onLogin: (username: string, password: string) => void;
}

interface State {
	isLoginDialogVisible: boolean;
}

class Main extends React.Component<Properties, State> {
	state: State = { isLoginDialogVisible: false };

	render() {
		const { isLoading, user } = this.props;
		return isLoading ? (
			<ContentDiv>
				<Spinner size="large" />
			</ContentDiv>
		) : (
			<>
				<PageLayout
					storeName="testStore"
					onLoginClick={this.toggleLoginDialog}
					username={user ? user.username : null}
				>
					<BrowserRouter>
						<Content />
					</BrowserRouter>
				</PageLayout>
				<LoginDialog
					isOpen={this.state.isLoginDialogVisible}
					onClose={this.toggleLoginDialog}
					onLogin={this.onLogin}
				/>
			</>
		);
	}

	private toggleLoginDialog = () => {
		this.setState({
			isLoginDialogVisible: !this.state.isLoginDialogVisible
		});
	};

	private onLogin = (username: string, password: string) => {
		this.toggleLoginDialog();
		this.props.onLogin(username, password);
	};
}

const stateToProps = ({ isLoading, user }: ReduxState) => ({
	isLoading,
	user
});
const dispatchToProps = (dispatch: Dispatch) => ({
	onLogin: async (username: string, password: string) => {
		setState(dispatch, { isLoading: true });
		try {
			await login(username, password);
			setState(dispatch, { isLoading: false, user: { username } });
		} catch (error) {
			// tslint:disable-next-line:no-console
			console.error(error);
			setState(dispatch, { isLoading: false });
		}
	}
});

const MainContainer = connect(
	stateToProps,
	dispatchToProps
)(Main);

export default MainContainer;
