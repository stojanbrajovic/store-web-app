import * as React from 'react';
import * as ReactDOM from 'react-dom';

const Tst: React.SFC = () => <span>Test</span>;

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(<Tst />, div);
	ReactDOM.unmountComponentAtNode(div);
});
