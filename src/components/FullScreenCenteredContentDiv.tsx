import * as React from "react";

const FullScreenCenteredContentDiv: React.SFC<{}> = props => (
	<div
		style={{
			display: "flex",
			justifyContent: "center",
			alignItems: "center",
			height: "100vh"
		}}
	>
		{props.children}
	</div>
);

export default FullScreenCenteredContentDiv;
